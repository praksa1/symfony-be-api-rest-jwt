<?php

namespace App\Controller;

use App\Entity\Preference;
use App\Entity\Task;
use App\Entity\TaskList;
use App\Repository\TaskListRepository;
use App\Repository\TaskRepository;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Controller\AbstractFOSRestController;

class ListController extends AbstractFOSRestController
{

    private $taskListRepository;
    private $taskRepository;
    private $em;
    public function __construct(TaskListRepository $taskListRepository, TaskRepository $taskRepository, EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->taskListRepository = $taskListRepository;
        $this->taskRepository = $taskRepository;
    }

    public function getListsAction()
    {
        $data = $this->taskListRepository->findAll();

        return $this->view($data, Response::HTTP_OK);
    }

    public function getListAction(TaskList $list)
    {
        $data = $list;

        return $this->view($data, Response::HTTP_OK);
    }
    public function getListsTasksAction(int $id)
    {
        $list = $this->taskListRepository->find($id);
        return $this->view($list->getTasks(), Response::HTTP_OK);
    }

    /**
     * @Rest\RequestParam(name="title", description="Title of the list", nullable=false)
     */
    public function postListsAction(ParamFetcher $paramFetcher)
    {
        $title = $paramFetcher->get('title');
        if ($title) {
            $list = new TaskList();

            $preferences = new Preference();

            $preferences->setList($list);
            $list->setPreferences($preferences);

            $list->setTitle($title);

            $this->em->persist($list);
            $this->em->flush();

            return $this->view($list, Response::HTTP_CREATED);
        }

        return $this->view(['title' => 'This cannot be null'], Response::HTTP_BAD_REQUEST);
    }

    public function patchListsAction(int $id)
    {
    }
    /**
     * @Rest\FileParam(name="image", description="The background of the list", nullable=false, image=true)
     */
    public function backgroundListsAction(Request $request, TaskList $list, ParamFetcher $paramFetcher)
    {
        $currentBackground = $list->getBackground();
        if (!is_null($currentBackground)) {
            $fileSystem = new Filesystem();
            $fileSystem->remove(
                $this->getUploadDirectory() . $currentBackground
            );
        }
        $file = ($paramFetcher->get('image'));

        if ($file) {
            $filename = md5(uniqid()) . '.png';

            $file->move(
                $this->getUploadDirectory(),
                $filename
            );

            $list->setBackground($filename);
            $list->setBackgroundPath('/uploads/' . $filename);

            $this->em->persist($list);
            $this->em->flush();

            $data = $request->getUriForPath(
                $list->getBackgroundPath()
            );

            return $this->view($data, Response::HTTP_OK);
        }

        return $this->view(['message' => 'Something went wrong'], Response::HTTP_BAD_REQUEST);
    }

    public function deleteListAction(int $id)
    {
        $list = $this->taskListRepository->find($id);

        $this->em->remove($list);
        $this->em->flush();

        return $this->view(null, Response::HTTP_NO_CONTENT);
    }
    /**
     * @Rest\RequestParam(name="title", description="The new title for the list", nullable=false)
     */
    public function patchListTitleAction(TaskList $list, ParamFetcher $paramFetcher)
    {
        $errors = [];

        $title = $paramFetcher->get('title');

        if (trim($title) !== '') {
            if ($list) {

                $list->setTitle($title);
                $this->em->persist($list);
                $this->em->flush();

                return $this->view(null, Response::HTTP_NO_CONTENT);
            }
            $errors[] = [
                'title' => 'This value cannot be empty'
            ];
        }
        $errors[] = [
            'title' => 'This value cannot be empty'
        ];

        return $this->view($errors, Response::HTTP_NO_CONTENT);
    }

    /**
     * @Rest\RequestParam(name="title", description="Title of the new task", nullable=false)
     */
    public function postListTasksAction(TaskList $list, ParamFetcher $paramFetcher)
    {

        if ($list) {
            $title = $paramFetcher->get('title');

            $task = new Task();
            $task->setTitle($title);

            $list->addTask($task);

            $this->em->persist($task);
            $this->em->flush();

            return $this->view($task, Response::HTTP_OK);
        }
        return $this->view(['message' => 'Something went wrong'], Response::HTTP_INTERNAL_SERVER_ERROR);
    }


    private function getUploadDirectory()
    {
        return $this->getParameter('uploads_dir');
    }
}