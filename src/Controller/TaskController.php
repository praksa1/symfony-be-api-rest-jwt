<?php

namespace App\Controller;

use App\Entity\Note;
use App\Entity\Task;
use App\Repository\TaskRepository;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\AbstractFOSRestController;

class TaskController extends AbstractFOSRestController
{
    private $taskRepository;
    private $em;
    public function __construct(TaskRepository $taskRepository, EntityManagerInterface $em)
    {
        $this->taskRepository = $taskRepository;
        $this->em = $em;
    }

    public function getTaskAction(Task $task)
    {
        return $this->view($task, Response::HTTP_OK);
    }
    public function deleteTaskAction(int $id)
    {
        $task = $this->taskRepository->find($id);

        if ($task) {

            $this->em->remove($task);
            $this->em->flush();

            return $this->view(null, Response::HTTP_NO_CONTENT);
        }
        return $this->view(['message' => 'Something went wrong'], Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    public function statusTaskAction(Task $task)
    {

        if ($task) {
            $task->setIsComplete(!$task->getIsComplete());
            $this->em->persist($task);
            $this->em->flush();

            return $this->view($task->getIsComplete(), Response::HTTP_NO_CONTENT);
        }
        return $this->view(['message' => 'Something went wrong'], Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    public function getTaskNotesAction(Task $task)
    {

        if ($task) {
            return $this->view($task->getNotes(), Response::HTTP_OK);
        }

        return $this->view(['message' => 'Something went wrong'], Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    /**
     * @Rest\RequestParam(name="note", description="Note for the task", nullable=false)
     */
    public function postTaskNoteAction(Task $task, ParamFetcher $paramFetcher)
    {

        if ($task) {
            $noteParam = $paramFetcher->get('note');
            $note = new Note();

            $note->setNote($noteParam);
            $note->setTask($task);

            $task->addNote($note);

            $this->em->persist($note);
            $this->em->flush();

            return $this->view($note, Response::HTTP_OK);
        }
        return $this->view(['message' => 'Something went wrong'], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
}