<?php

namespace App\Controller;

use App\Entity\Note;
use App\Repository\NoteRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\AbstractFOSRestController;

class NoteController extends AbstractFOSRestController
{
    private $noteRepository;
    private $em;
    public function __construct(NoteRepository $noteRepository, EntityManagerInterface $em)
    {
        $this->noteRepository = $noteRepository;
        $this->em = $em;
    }

    public function getNoteAction(Note $note)
    {

        return $this->view($note, Response::HTTP_OK);
    }
    public function deleteNoteAction(Note $note)
    {

        if ($note) {
            $this->em->remove($note);
            $this->em->flush();

            return $this->view(null, Response::HTTP_NO_CONTENT);
        }
        return $this->view(['message' => 'Something went wrong'], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
}