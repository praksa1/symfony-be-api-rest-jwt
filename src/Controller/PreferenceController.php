<?php

namespace App\Controller;

use App\Entity\TaskList;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;

class PreferenceController extends AbstractFOSRestController
{
    private $em;
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function getPreferencesAction(TaskList $list)
    {
        return $this->view($list->getPreferences(), Response::HTTP_OK);
    }
    /**
     * @Rest\RequestParam(name="sortValue", description="The value will be used to sort the list", nullable=false)
     */
    public function sortPreferenceAction(ParamFetcher $paramFetcher, TaskList $list)
    {
        $sortValue = $paramFetcher->get('sortValue');

        if ($sortValue) {
            $list->getPreferences()->setSortValue($sortValue);
            $this->em->persist($list);
            $this->em->flush();

            return $this->view(null, Response::HTTP_NO_CONTENT);
        }

        $data['code'] = Response::HTTP_CONFLICT;
        $data['message'] = 'The sort value cannot be null';
        return $this->view($data, Response::HTTP_CONFLICT);
    }

    /**
     * @Rest\RequestParam(name="filterValue", description="The filter value", nullable=false)
     */
    public function filterPreferencesAction(ParamFetcher $paramFetcher, TaskList $list)
    {
        $filterValue = $paramFetcher->get('filterValue');

        if ($filterValue) {
            $list->getPreferences()->setFilterValue($filterValue);
            $this->em->persist($list);
            $this->em->flush();

            return $this->view(null, Response::HTTP_NO_CONTENT);
        }

        $data['code'] = Response::HTTP_CONFLICT;
        $data['message'] = 'The filter value cannot be null';
        return $this->view($data, Response::HTTP_CONFLICT);
    }
}